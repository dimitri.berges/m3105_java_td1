/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Geometry;

/**
 *
 * @author bergesd
 */
public class Point {
    
    /** Coordonnées du point */
    private int x, y;
    
    /** Constructeur en fonction de coordonnées
     * @param x
     * @param y */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }
    /** Constructeur en fonction d'un autre point
     * @param otherPoint */
    public Point(Point otherPoint) {
        this.x = otherPoint.x;
        this.y = otherPoint.y;
    }
    /** Constructeur en fonction d'un autre point plus d'autres coordonnées
     * @param otherPoint
     * @param x
     * @param y */
    public Point(Point otherPoint, int x, int y) {
        this.x = otherPoint.x + x;
        this.y = otherPoint.y + y;
    }
    /** Constructeur vide (0, 0) */
    public Point() {
        this.x = 0;
        this.y = 0;
    }
    
    /** Défini les coordonnées du point à X et Y
     * @param x
     * @param y */
    public void setCoords(int x, int y) {
        this.x = x;
        this.y = y;
    }
    /** Défini les coordonnées du point en fonction d'un autre point
     * @param otherPoint */
    public void setCoords(Point otherPoint) {
        this.x = otherPoint.x;
        this.y = otherPoint.y;
    }
    /** Défini les coordonnées du point en fonction d'un autre point plus d'autres coordonnées
     * @param otherPoint
     * @param x
     * @param y */
    public void setCoords(Point otherPoint, int x, int y) {
        this.x = otherPoint.x + x;
        this.y = otherPoint.y + y;
    }
    
    /** Défini X
     * @param x */
    public void setX(int x) {this.x = x;}
    /** Défini Y
     * @param y */
    public void setY(int y) {this.y = y;}
    
    /** Retourne X
     * @return  */
    public int getX() {return this.x;}
    /** Retourne Y
     * @return  */
    public int getY() {return this.y;}
    
    /** Retourne les coordonnées du point
     * @return  */
    @Override
    public String toString() {
        return "{X = " + x + "; Y = " + y + "}";
    }
    
}
