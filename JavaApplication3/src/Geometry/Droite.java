/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geometrie;
import java.lang.Exception;
import java.util.Objects;

/**
 *
 * @author bergesd
 */
public class Droite {
    
    /*Attributs*/
    private Point A, B;
    
    /* Constructeurs */
    /**
     * Initialise la droite avec 2 points.
     * 
     * @param a
     * @param b 
     * @throws java.lang.Exception 
     */
    public Droite(Point a, Point b) throws Exception {
        if (a == null || b == null) {throw new Exception();}
        if (a == b) {throw new Exception();}
        A = a;
        B = b;
    }
    /**
     * Initialise la droite avec un point et l'origine
     * @param a 
     * @throws java.lang.Exception 
     */
    public Droite(Point a) throws Exception {
        if (a == null) {throw new Exception();}
        if (a.getAbscisse() == (byte)0 && a.getOrdonnee() == (byte)0) {throw new Exception();}
        A = a;
        B = new Point((byte)0, (byte)0);
    }
    public Droite() {
        A = new Point();
        B = new Point((byte)1, (byte)1);
    }
    
    
    /* Méthodes */

    public Point getA() {
        return A;
    }

    public void setA(Point A) throws Exception {
        if (A == null) {throw new Exception();}
        if (A == this.B) {throw new Exception();}
        this.A = A;
    }

    public Point getB() {
        return B;
    }

    public void setB(Point B) throws Exception {
        if (B == null) {throw new Exception();}
        if (B == this.A) {throw new Exception();}
        this.B = B;
    }

    @Override
    public String toString() {
        return "Droite{" + "A=" + A + ", B=" + B + '}';
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Droite other = (Droite) obj;
        if (!Objects.equals(this.A, other.A)) {
            return false;
        }
        if (!Objects.equals(this.B, other.B)) {
            return false;
        }
        return true;
    }
}
